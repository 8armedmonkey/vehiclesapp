package nl.parkmobile.assignment.vehiclesapp

import android.app.Application
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.di.DependencyInjectionHelper

class VehiclesApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        DependencyInjectionHelper.init()
    }

}