package nl.parkmobile.assignment.vehiclesapp.common.appservice


interface Query<out Result> {

    fun execute(): Result

}