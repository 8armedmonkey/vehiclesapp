package nl.parkmobile.assignment.vehiclesapp.common.infrastructure.di

import nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.di.VehicleModule

object DependencyInjectionHelper {

    lateinit var di: DependencyInjector
        private set

    fun init() {
        di = DaggerDependencyInjector
            .builder()
            .vehicleModule(VehicleModule)
            .build()
    }

}