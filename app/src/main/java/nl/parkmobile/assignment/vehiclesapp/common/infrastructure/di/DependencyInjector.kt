package nl.parkmobile.assignment.vehiclesapp.common.infrastructure.di

import dagger.Component
import nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.di.VehicleModule
import nl.parkmobile.assignment.vehiclesapp.vehicles.presentation.VehicleDetailPresenter
import nl.parkmobile.assignment.vehiclesapp.vehicles.presentation.VehiclesPresenter
import javax.inject.Singleton


@Component(modules = [VehicleModule::class, RxModule::class])
@Singleton
interface DependencyInjector {

    fun inject(presenter: VehiclesPresenter)

    fun inject(presenter: VehicleDetailPresenter)

}