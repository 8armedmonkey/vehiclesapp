package nl.parkmobile.assignment.vehiclesapp.common.infrastructure.di

import dagger.Module
import dagger.Provides
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx.SchedulingTransformer
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx.SchedulingTransformerImpl
import javax.inject.Singleton


@Module
class RxModule {

    @Provides
    @Singleton
    fun provideSchedulingTransformer(): SchedulingTransformer = SchedulingTransformerImpl()

}