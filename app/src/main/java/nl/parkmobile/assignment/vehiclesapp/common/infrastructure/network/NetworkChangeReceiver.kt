package nl.parkmobile.assignment.vehiclesapp.common.infrastructure.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.ConnectivityManager.CONNECTIVITY_ACTION

class NetworkChangeReceiver(
    private val callback: NetworkActiveCallback

) : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val connectivityManager = context?.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo

        if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
            callback.onNetworkActive()
        }
    }

    fun register(context: Context) {
        context.registerReceiver(this, IntentFilter(CONNECTIVITY_ACTION))
    }

    fun unregister(context: Context) {
        context.unregisterReceiver(this)
    }

    interface NetworkActiveCallback {

        fun onNetworkActive()

    }

}