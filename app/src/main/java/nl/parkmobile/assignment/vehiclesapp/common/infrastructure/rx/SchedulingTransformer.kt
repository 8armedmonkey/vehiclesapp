package nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx

import io.reactivex.ObservableTransformer

interface SchedulingTransformer {

    fun <T> applyBackgroundAndMain(): ObservableTransformer<T, T>

}