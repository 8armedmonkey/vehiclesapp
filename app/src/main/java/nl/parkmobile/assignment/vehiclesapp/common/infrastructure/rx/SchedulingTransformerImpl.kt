package nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx

import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulingTransformerImpl : SchedulingTransformer {

    override fun <T> applyBackgroundAndMain(): ObservableTransformer<T, T> =
        ObservableTransformer { upstream ->

            upstream
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

        }

}