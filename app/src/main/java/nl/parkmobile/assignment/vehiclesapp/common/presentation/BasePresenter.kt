package nl.parkmobile.assignment.vehiclesapp.common.presentation

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BasePresenter<out View>(val view: View) {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
        @Synchronized get
        @Synchronized set

    abstract fun start()

    open fun stop() {
        compositeDisposable.dispose()
    }

    @Synchronized
    protected fun addDisposable(disposable: Disposable) {
        if (compositeDisposable.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }

        compositeDisposable.add(disposable)
    }

}