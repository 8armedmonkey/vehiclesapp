package nl.parkmobile.assignment.vehiclesapp.common.presentation

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import nl.parkmobile.assignment.vehiclesapp.R

class LoadingOverlay(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    init {
        View.inflate(context, R.layout.view_loading_overlay, this)
    }

}