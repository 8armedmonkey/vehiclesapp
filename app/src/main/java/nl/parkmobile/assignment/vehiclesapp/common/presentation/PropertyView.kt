package nl.parkmobile.assignment.vehiclesapp.common.presentation

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_property.view.*
import nl.parkmobile.assignment.vehiclesapp.R


class PropertyView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    @Suppress("MemberVisibilityCanBePrivate")
    var propertyName: String? = null
        set(value) {
            field = value
            textPropertyName.text = value
        }

    @Suppress("MemberVisibilityCanBePrivate")
    var propertyValue: String? = null
        set(value) {
            field = value
            textPropertyValue.text = value
        }

    init {
        View.inflate(context, R.layout.view_property, this)
        orientation = HORIZONTAL

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.PropertyView, 0, 0)
        propertyName = a.getString(R.styleable.PropertyView_propertyName)
        propertyValue = a.getString(R.styleable.PropertyView_propertyValue)

        a.recycle()
    }

}