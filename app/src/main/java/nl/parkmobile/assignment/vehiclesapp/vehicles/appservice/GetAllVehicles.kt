package nl.parkmobile.assignment.vehiclesapp.vehicles.appservice

import io.reactivex.Observable
import nl.parkmobile.assignment.vehiclesapp.common.appservice.Query
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepository
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepositoryException
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles


class GetAllVehicles(
    private val vehicleRepository: VehicleRepository

) : Query<Observable<Vehicles>> {

    override fun execute(): Observable<Vehicles> = Observable.create { emitter ->

        try {
            emitter.onNext(vehicleRepository.getAll())
            emitter.onComplete()

        } catch (e: VehicleRepositoryException) {
            emitter.onError(e)
        }

    }


}