package nl.parkmobile.assignment.vehiclesapp.vehicles.appservice

import io.reactivex.Observable
import nl.parkmobile.assignment.vehiclesapp.common.appservice.Query
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepository
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepositoryException

class GetVehicleDetail(
    private val vehicleRepository: VehicleRepository,
    private val vehicleId: Int

) : Query<Observable<Vehicle>> {

    override fun execute(): Observable<Vehicle> = Observable.create { emitter ->

        try {
            emitter.onNext(vehicleRepository.get(vehicleId))
            emitter.onComplete()

        } catch (e: VehicleRepositoryException) {
            emitter.onError(e)
        }

    }

}