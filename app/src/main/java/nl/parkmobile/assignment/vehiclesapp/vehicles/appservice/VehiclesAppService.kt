package nl.parkmobile.assignment.vehiclesapp.vehicles.appservice

import io.reactivex.Observable
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles


interface VehiclesAppService {

    fun getAll(): Observable<Vehicles>

    fun get(vehicleId: Int): Observable<Vehicle>

}