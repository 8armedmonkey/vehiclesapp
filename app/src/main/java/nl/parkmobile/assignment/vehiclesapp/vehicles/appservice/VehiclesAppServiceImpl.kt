package nl.parkmobile.assignment.vehiclesapp.vehicles.appservice

import io.reactivex.Observable
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepository
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles

class VehiclesAppServiceImpl(
    private val vehicleRepository: VehicleRepository

) : VehiclesAppService {

    override fun getAll(): Observable<Vehicles> =
        GetAllVehicles(vehicleRepository).execute()

    override fun get(vehicleId: Int): Observable<Vehicle> =
        GetVehicleDetail(vehicleRepository, vehicleId).execute()

}