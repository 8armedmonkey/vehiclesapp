package nl.parkmobile.assignment.vehiclesapp.vehicles.domain


data class Vehicle(
    val id: Int,
    val vrn: String,
    val country: String,
    val color: String,
    val type: VehicleType,
    val default: Boolean
)
