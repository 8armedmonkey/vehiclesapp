package nl.parkmobile.assignment.vehiclesapp.vehicles.domain


interface VehicleRepository {

    @Throws(VehicleRepositoryException::class)
    fun getAll(): Vehicles

    @Throws(VehicleRepositoryException::class)
    fun get(vehicleId: Int): Vehicle

}