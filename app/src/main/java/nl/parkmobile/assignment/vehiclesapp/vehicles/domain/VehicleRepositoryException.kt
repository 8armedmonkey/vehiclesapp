package nl.parkmobile.assignment.vehiclesapp.vehicles.domain


class VehicleRepositoryException : Exception {

    constructor()

    constructor(cause: Throwable) : super(cause)

}