package nl.parkmobile.assignment.vehiclesapp.vehicles.domain


enum class VehicleType(val value: String) {
    CAR("Car"),
    TRUCK("Truck"),
    RV("RV");

    companion object {
        fun from(value: String): VehicleType {
            val vehicleType = values().find { it.value == value }

            if (vehicleType != null) {
                return vehicleType
            } else {
                throw IllegalArgumentException()
            }
        }
    }
}