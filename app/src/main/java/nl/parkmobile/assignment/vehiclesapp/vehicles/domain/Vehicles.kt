package nl.parkmobile.assignment.vehiclesapp.vehicles.domain


class Vehicles(vararg vehicles: Vehicle) : Iterable<Vehicle> {

    private val entries: MutableList<Vehicle> = mutableListOf()
    private val lookup: MutableMap<Int, Vehicle> = mutableMapOf()

    init {
        entries.addAll(vehicles)
        entries.forEach { lookup[it.id] = it }
    }

    override fun iterator(): Iterator<Vehicle> = entries.iterator()

    fun get(id: Int): Vehicle? = lookup[id]

}
