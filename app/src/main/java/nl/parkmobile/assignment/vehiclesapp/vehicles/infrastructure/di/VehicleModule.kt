package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.di

import dagger.Module
import dagger.Provides
import nl.parkmobile.assignment.vehiclesapp.BuildConfig
import nl.parkmobile.assignment.vehiclesapp.vehicles.appservice.VehiclesAppService
import nl.parkmobile.assignment.vehiclesapp.vehicles.appservice.VehiclesAppServiceImpl
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepository
import nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl.VehicleRepositoryImpl
import nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl.VehicleWebService
import nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl.VehicleWebServiceFactory
import javax.inject.Singleton


@Module
object VehicleModule {

    @Provides
    @Singleton
    internal fun provideVehicleWebService(): VehicleWebService =
        VehicleWebServiceFactory.create()

    @Provides
    @Singleton
    internal fun provideVehicleRepository(vehicleWebService: VehicleWebService): VehicleRepository =
        VehicleRepositoryImpl(BuildConfig.CACHE_MAX_AGE_MILLIS, vehicleWebService)

    @Provides
    @Singleton
    internal fun provideVehicleAppService(vehicleRepository: VehicleRepository): VehiclesAppService =
        VehiclesAppServiceImpl(vehicleRepository)
}