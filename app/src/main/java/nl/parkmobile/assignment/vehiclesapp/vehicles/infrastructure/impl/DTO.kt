package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl

import java.util.*


object DTO {

    data class Vehicles(val vehicles: Array<Vehicle>) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Vehicles

            if (!Arrays.equals(vehicles, other.vehicles)) return false

            return true
        }

        override fun hashCode(): Int {
            return Arrays.hashCode(vehicles)
        }

    }

    data class Vehicle(
        val vehicleId: Int,
        val vrn: String,
        val country: String,
        val color: String,
        val type: String,
        val default: Boolean
    )

}