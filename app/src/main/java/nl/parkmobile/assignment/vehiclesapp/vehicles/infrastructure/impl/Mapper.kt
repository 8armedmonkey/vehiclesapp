package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl

import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles

object Mapper {

    fun map(dto: DTO.Vehicles): Vehicles =
        Vehicles(*(dto.vehicles.map { map(it) }.toTypedArray()))

    fun map(dto: DTO.Vehicle): Vehicle =
        Vehicle(
            dto.vehicleId,
            dto.vrn,
            dto.country,
            dto.color,
            VehicleType.from(dto.type),
            dto.default
        )

}