package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl

import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepository
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepositoryException
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles
import java.util.concurrent.TimeUnit


class VehicleRepositoryImpl(
    private val cacheMaxAgeMillis: Long,
    private val vehicleWebService: VehicleWebService

) : VehicleRepository {

    internal var cachedVehicles: Vehicles? = null
        @Synchronized get
        @Synchronized set

    internal var lastUpdatedMillis: Long = 0
        @Synchronized get
        @Synchronized set

    override fun getAll(): Vehicles {
        updateCacheIfNeeded()
        return returnCachedVehicles()
    }

    override fun get(vehicleId: Int): Vehicle {
        updateCacheIfNeeded()
        return returnCachedVehicle(vehicleId)
    }

    @Throws(VehicleRepositoryException::class)
    @Synchronized
    private fun updateCacheIfNeeded() {
        val currentTimeMillis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime())

        if (isCacheInvalid(currentTimeMillis)) {
            try {
                val vehicles = getAllInternal()

                cachedVehicles = vehicles
                lastUpdatedMillis = currentTimeMillis

            } catch (e: VehicleRepositoryException) {
                rethrowExceptionIfCacheUnavailable(e)
            }
        }
    }

    @Throws(VehicleRepositoryException::class)
    @Synchronized
    private fun getAllInternal(): Vehicles? =
        try {
            val dto = vehicleWebService.getAll().execute().body()
            Mapper.map(dto!!)

        } catch (e: Exception) {
            throw VehicleRepositoryException(e)
        }

    @Throws(VehicleRepositoryException::class)
    private fun returnCachedVehicles(): Vehicles =
        try {
            cachedVehicles!!

        } catch (e: Exception) {
            throw VehicleRepositoryException(e)
        }

    @Throws(VehicleRepositoryException::class)
    private fun returnCachedVehicle(vehicleId: Int): Vehicle =
        try {
            val cachedVehicle = cachedVehicles?.get(vehicleId)
            cachedVehicle!!

        } catch (e: Exception) {
            throw VehicleRepositoryException(e)
        }

    @Throws(VehicleRepositoryException::class)
    private fun rethrowExceptionIfCacheUnavailable(e: VehicleRepositoryException) {
        if (isCacheUnavailable()) {
            throw e
        }
    }

    private fun isCacheInvalid(currentTimeMillis: Long) =
        isCacheUnavailable() || isCacheExpired(currentTimeMillis)

    private fun isCacheUnavailable() = cachedVehicles == null

    private fun isCacheExpired(currentTimeMillis: Long) =
        currentTimeMillis - lastUpdatedMillis > cacheMaxAgeMillis

}