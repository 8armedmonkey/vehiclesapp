package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl

import retrofit2.Call
import retrofit2.http.GET


interface VehicleWebService {

    @GET("/vehicles")
    @Throws(Exception::class)
    fun getAll(): Call<DTO.Vehicles>

}