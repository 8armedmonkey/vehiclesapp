package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl

import nl.parkmobile.assignment.vehiclesapp.BuildConfig
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object VehicleWebServiceFactory {

    fun create(): VehicleWebService = Retrofit.Builder()
        .baseUrl(BuildConfig.WEB_SERVICE_BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(VehicleWebService::class.java)

}