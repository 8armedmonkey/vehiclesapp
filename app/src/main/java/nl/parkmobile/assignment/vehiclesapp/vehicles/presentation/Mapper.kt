package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import nl.parkmobile.assignment.vehiclesapp.R
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles

object Mapper {

    fun map(vehicles: Vehicles): VehiclesPresentation =
        VehiclesPresentation(*(vehicles.map { map(it) }.toTypedArray()))

    fun map(vehicle: Vehicle): VehiclePresentation =
        VehiclePresentation(
            vehicle.id,
            vehicle.vrn,
            vehicle.country,
            vehicle.color,
            vehicle.type,
            vehicle.default,
            map(vehicle.type))

    private fun map(type: VehicleType): Int =
        when (type) {
            VehicleType.CAR -> R.drawable.ic_car
            VehicleType.TRUCK -> R.drawable.ic_truck
            VehicleType.RV -> R.drawable.ic_rv
        }

}