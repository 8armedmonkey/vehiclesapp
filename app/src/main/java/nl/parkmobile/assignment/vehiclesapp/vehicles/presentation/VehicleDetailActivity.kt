package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import nl.parkmobile.assignment.vehiclesapp.R
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.di.DependencyInjectionHelper
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.network.NetworkChangeReceiver

class VehicleDetailActivity : AppCompatActivity(), NetworkChangeReceiver.NetworkActiveCallback {

    companion object {

        private const val EXTRA_VEHICLE_ID = "vehicleId"

        fun getStartIntent(context: Context, vehicleId: Int): Intent {
            val intent = Intent(context, VehicleDetailActivity::class.java)
            intent.putExtra(EXTRA_VEHICLE_ID, vehicleId)

            return intent
        }

    }

    private lateinit var view: VehicleDetailView
    private lateinit var presenter: VehicleDetailPresenter
    private lateinit var networkChangeReceiver: NetworkChangeReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle_detail)

        view = findViewById<VehicleDetailViewImpl>(R.id.viewVehicleDetail)
        presenter = VehicleDetailPresenter(view, intent.getIntExtra(EXTRA_VEHICLE_ID, 0))
        networkChangeReceiver = NetworkChangeReceiver(this)

        DependencyInjectionHelper.di.inject(presenter)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onResume() {
        super.onResume()
        networkChangeReceiver.register(this)
    }

    override fun onPause() {
        networkChangeReceiver.unregister(this)
        super.onPause()
    }

    override fun onStop() {
        presenter.stop()
        super.onStop()
    }

    override fun onNetworkActive() {
        presenter.onNetworkActive()
    }

}