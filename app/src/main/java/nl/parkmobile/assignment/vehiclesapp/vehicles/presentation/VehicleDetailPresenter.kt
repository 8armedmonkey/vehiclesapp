package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx.SchedulingTransformer
import nl.parkmobile.assignment.vehiclesapp.common.presentation.BasePresenter
import nl.parkmobile.assignment.vehiclesapp.vehicles.appservice.VehiclesAppService
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import javax.inject.Inject


class VehicleDetailPresenter(
    view: VehicleDetailView,
    private val vehicleId: Int

) : BasePresenter<VehicleDetailView>(view) {

    @Inject
    internal lateinit var vehiclesAppService: VehiclesAppService

    @Inject
    internal lateinit var schedulingTransformer: SchedulingTransformer


    override fun start() {
        getVehicleDetail()
    }

    fun onNetworkActive() {
        getVehicleDetail()
    }

    private fun getVehicleDetail() {
        addDisposable(
            vehiclesAppService.get(vehicleId)
                .compose(schedulingTransformer.applyBackgroundAndMain())
                .subscribe(
                    this::onGetVehicleDetailSuccess,
                    this::onGetVehicleDetailFailed,
                    {},
                    { onGetVehicleDetailStarted() }
                )
        )
    }

    private fun onGetVehicleDetailStarted() {
        view.showLoading()
    }

    private fun onGetVehicleDetailSuccess(vehicle: Vehicle) {
        view.showVehicleDetail(Mapper.map(vehicle))
    }

    private fun onGetVehicleDetailFailed(throwable: Throwable) {
        view.showError(throwable)
    }

}
