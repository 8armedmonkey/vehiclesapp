package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation


interface VehicleDetailView {

    fun showLoading()

    fun showVehicleDetail(vehicle: VehiclePresentation)

    fun showError(throwable: Throwable)

}