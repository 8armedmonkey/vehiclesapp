package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import android.content.Context
import android.support.design.widget.Snackbar
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.view_vehicle_detail.view.*
import nl.parkmobile.assignment.vehiclesapp.R


class VehicleDetailViewImpl(context: Context, attrs: AttributeSet) :
    FrameLayout(context, attrs), VehicleDetailView {

    private var snackbar: Snackbar? = null

    init {
        View.inflate(context, R.layout.view_vehicle_detail, this)
    }

    override fun showLoading() {
        loadingOverlay.visibility = View.VISIBLE

        hideError()
    }

    override fun showVehicleDetail(vehicle: VehiclePresentation) {
        loadingOverlay.visibility = View.GONE

        propertyVehicleRegistrationNumber.propertyValue = vehicle.vrn
        propertyCountry.propertyValue = vehicle.country
        propertyColor.propertyValue = vehicle.color
        propertyType.propertyValue = vehicle.type.value

        hideError()
    }

    override fun showError(throwable: Throwable) {
        loadingOverlay.visibility = View.GONE

        snackbar = Snackbar.make(this, R.string.err_fetching_data, Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
    }

    private fun hideError() {
        snackbar?.dismiss()
        snackbar = null
    }

}