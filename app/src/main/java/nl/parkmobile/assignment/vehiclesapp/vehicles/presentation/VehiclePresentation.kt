package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType

data class VehiclePresentation(
    val id: Int,
    val vrn: String,
    val country: String,
    val color: String,
    val type: VehicleType,
    val default: Boolean,
    val iconResourceId: Int
)