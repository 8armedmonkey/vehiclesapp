package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import nl.parkmobile.assignment.vehiclesapp.R
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.di.DependencyInjectionHelper
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.network.NetworkChangeReceiver


class VehiclesActivity :
    AppCompatActivity(), NetworkChangeReceiver.NetworkActiveCallback, VehiclesNavigator {

    private lateinit var view: VehiclesView
    private lateinit var presenter: VehiclesPresenter
    private lateinit var networkChangeReceiver: NetworkChangeReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicles)

        view = findViewById<VehiclesViewImpl>(R.id.viewVehicles)
        presenter = VehiclesPresenter(view, this)
        networkChangeReceiver = NetworkChangeReceiver(this)

        DependencyInjectionHelper.di.inject(presenter)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onResume() {
        super.onResume()
        networkChangeReceiver.register(this)
    }

    override fun onPause() {
        networkChangeReceiver.unregister(this)
        super.onPause()
    }

    override fun onStop() {
        presenter.stop()
        super.onStop()
    }

    override fun onNetworkActive() {
        presenter.onNetworkActive()
    }

    override fun goToVehicleDetail(vehicleId: Int) {
        startActivity(VehicleDetailActivity.getStartIntent(this, vehicleId))
    }

}