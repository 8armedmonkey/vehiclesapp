package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import nl.parkmobile.assignment.vehiclesapp.R

class VehiclesAdapter(
    private val onItemClickListener: VehiclesAdapter.OnItemClickListener

) : RecyclerView.Adapter<VehiclesAdapter.ViewHolder>() {

    internal var vehiclesPresentation: VehiclesPresentation? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_vehicle, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = vehiclesPresentation?.size() ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val textVehicleRegistrationNumber: TextView = holder.itemView as TextView

        textVehicleRegistrationNumber.text = vehiclesPresentation?.getAt(position)?.vrn
        textVehicleRegistrationNumber.setOnClickListener {

            val vehicleId = vehiclesPresentation?.getAt(position)?.id

            if (vehicleId != null) {
                onItemClickListener.onItemClick(vehicleId)
            }

        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface OnItemClickListener {

        fun onItemClick(vehicleId: Int)

    }

}