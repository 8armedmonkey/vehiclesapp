package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

interface VehiclesNavigator {

    fun goToVehicleDetail(vehicleId: Int)

}