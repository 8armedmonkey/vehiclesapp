package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

class VehiclesPresentation(vararg vehicles: VehiclePresentation) : Iterable<VehiclePresentation> {

    private val entries: MutableList<VehiclePresentation> = mutableListOf()

    init {
        entries.addAll(vehicles)
    }

    override fun iterator(): Iterator<VehiclePresentation> = entries.iterator()

    fun getAt(index: Int): VehiclePresentation? = entries[index]

    fun size(): Int = entries.size

}