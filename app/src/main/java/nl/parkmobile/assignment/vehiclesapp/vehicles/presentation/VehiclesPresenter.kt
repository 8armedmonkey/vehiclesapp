package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx.SchedulingTransformer
import nl.parkmobile.assignment.vehiclesapp.common.presentation.BasePresenter
import nl.parkmobile.assignment.vehiclesapp.vehicles.appservice.VehiclesAppService
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles
import javax.inject.Inject


class VehiclesPresenter(
    view: VehiclesView,
    private val navigator: VehiclesNavigator

) : BasePresenter<VehiclesView>(view), VehiclesView.Listener {

    @Inject
    internal lateinit var vehiclesAppService: VehiclesAppService

    @Inject
    internal lateinit var schedulingTransformer: SchedulingTransformer

    init {
        view.setListener(this)
    }

    override fun start() {
        getAllVehicles()
    }

    override fun onVehicleSelected(vehicleId: Int) {
        navigator.goToVehicleDetail(vehicleId)
    }

    fun onNetworkActive() {
        getAllVehicles()
    }

    private fun getAllVehicles() {
        addDisposable(
            vehiclesAppService.getAll()
                .compose(schedulingTransformer.applyBackgroundAndMain())
                .subscribe(
                    this::onGetAllVehiclesSuccess,
                    this::onGetAllVehiclesFailed,
                    {},
                    { onGetAllVehiclesStarted() }
                )
        )
    }

    private fun onGetAllVehiclesStarted() {
        view.showLoading()
    }

    private fun onGetAllVehiclesSuccess(vehicles: Vehicles) {
        view.showVehicles(Mapper.map(vehicles))
    }

    private fun onGetAllVehiclesFailed(throwable: Throwable) {
        view.showError(throwable)
    }

}