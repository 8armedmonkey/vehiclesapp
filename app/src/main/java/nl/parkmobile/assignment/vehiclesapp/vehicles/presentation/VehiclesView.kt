package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

interface VehiclesView {

    fun showLoading()

    fun showVehicles(vehicles: VehiclesPresentation)

    fun showError(throwable: Throwable)

    fun setListener(listener: Listener)

    interface Listener {

        fun onVehicleSelected(vehicleId: Int)

    }

}