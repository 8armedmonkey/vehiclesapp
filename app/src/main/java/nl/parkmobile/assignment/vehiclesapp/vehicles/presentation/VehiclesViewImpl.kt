package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.view_vehicles.view.*
import nl.parkmobile.assignment.vehiclesapp.R

class VehiclesViewImpl(context: Context, attrs: AttributeSet) :
    FrameLayout(context, attrs), VehiclesView, VehiclesAdapter.OnItemClickListener {

    private val adapter: VehiclesAdapter = VehiclesAdapter(this)
    private var snackbar: Snackbar? = null
    private var listener: VehiclesView.Listener? = null

    init {
        View.inflate(context, R.layout.view_vehicles, this)
        recyclerVehicles.layoutManager = LinearLayoutManager(context, VERTICAL, false)
        recyclerVehicles.adapter = adapter
    }

    override fun showLoading() {
        loadingOverlay.visibility = View.VISIBLE

        hideError()
    }

    override fun showVehicles(vehicles: VehiclesPresentation) {
        loadingOverlay.visibility = View.GONE

        adapter.vehiclesPresentation = vehicles
        adapter.notifyDataSetChanged()

        hideError()
    }

    override fun showError(throwable: Throwable) {
        loadingOverlay.visibility = View.GONE

        snackbar = Snackbar.make(this, R.string.err_fetching_data, Snackbar.LENGTH_INDEFINITE)
        snackbar?.show()
    }

    override fun setListener(listener: VehiclesView.Listener) {
        this.listener = listener
    }

    override fun onItemClick(vehicleId: Int) {
        listener?.onVehicleSelected(vehicleId)
    }

    private fun hideError() {
        snackbar?.dismiss()
        snackbar = null
    }

}