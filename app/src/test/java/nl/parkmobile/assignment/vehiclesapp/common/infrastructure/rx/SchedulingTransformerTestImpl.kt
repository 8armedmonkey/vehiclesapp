package nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx

import io.reactivex.ObservableTransformer

class SchedulingTransformerTestImpl : SchedulingTransformer {

    // No scheduling.
    override fun <T> applyBackgroundAndMain(): ObservableTransformer<T, T> =
        ObservableTransformer { upstream -> upstream }

}