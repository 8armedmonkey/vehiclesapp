package nl.parkmobile.assignment.vehiclesapp.common.presentation

import io.reactivex.disposables.Disposable

class MockPresenter(
    view: MockView,
    vararg disposables: Disposable

) : BasePresenter<MockView>(view) {

    init {
        disposables.forEach { addDisposable(it) }
    }

    override fun start() {
        // Do nothing.
    }

}