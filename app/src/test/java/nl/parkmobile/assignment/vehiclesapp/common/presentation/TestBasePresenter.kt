package nl.parkmobile.assignment.vehiclesapp.common.presentation

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.disposables.Disposable
import org.junit.Test


class TestBasePresenter {

    @Test
    fun stop_disposablesAreDisposed() {
        val d1: Disposable = mock()
        val d2: Disposable = mock()
        val d3: Disposable = mock()

        val presenter = MockPresenter(mock(), d1, d2, d3)
        presenter.stop()

        verify(d1).dispose()
        verify(d2).dispose()
        verify(d3).dispose()
    }

}