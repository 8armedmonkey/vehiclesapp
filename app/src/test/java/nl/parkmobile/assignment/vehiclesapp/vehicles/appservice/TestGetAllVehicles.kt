package nl.parkmobile.assignment.vehiclesapp.vehicles.appservice

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.observers.TestObserver
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class TestGetAllVehicles {

    private lateinit var vehicleRepository: VehicleRepository
    private lateinit var getAllVehicles: GetAllVehicles

    @Before
    fun setUp() {
        vehicleRepository = mock()
        getAllVehicles = GetAllVehicles(vehicleRepository)
    }

    @Test
    fun execute_repositoryWorksProperly_nextEventTriggered() {
        setupVehicleRepositoryWithSuccessResponse()

        val observer: TestObserver<Vehicles> = TestObserver.create()
        getAllVehicles.execute().subscribe(observer)

        observer.awaitTerminalEvent()
        observer.assertNoErrors()
        observer.assertValueCount(1)

        observer.values()[0].iterator().asSequence().forEachIndexed { index, vehicle ->
            when (index) {
                0 -> assertEquals(
                    Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true),
                    vehicle
                )
                1 -> assertEquals(
                    Vehicle(107523, "PARK02", "ES", "White", VehicleType.RV, false),
                    vehicle
                )
            }
        }
    }

    @Test
    fun execute_repositoryError_errorEventTriggered() {
        setupVehicleRepositoryWithFailedResponse()

        val observer: TestObserver<Vehicles> = TestObserver.create()
        getAllVehicles.execute().subscribe(observer)

        observer.assertError(VehicleRepositoryException::class.java)
    }

    private fun setupVehicleRepositoryWithSuccessResponse() {
        whenever(vehicleRepository.getAll()).thenReturn(Vehicles(
            Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true),
            Vehicle(107523, "PARK02", "ES", "White", VehicleType.RV, false)
        ))
    }

    private fun setupVehicleRepositoryWithFailedResponse() {
        whenever(vehicleRepository.getAll()).thenThrow(VehicleRepositoryException())
    }

}