package nl.parkmobile.assignment.vehiclesapp.vehicles.appservice

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.observers.TestObserver
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepository
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepositoryException
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class TestGetVehicleDetail {

    private lateinit var vehicleRepository: VehicleRepository
    private lateinit var getVehicleDetail: GetVehicleDetail

    @Before
    fun setUp() {
        vehicleRepository = mock()
        getVehicleDetail = GetVehicleDetail(vehicleRepository, 107623)
    }

    @Test
    fun execute_repositoryWorksProperly_nextEventTriggered() {
        setupVehicleRepositoryWithSuccessResponse()

        val observer: TestObserver<Vehicle> = TestObserver.create()
        getVehicleDetail.execute().subscribe(observer)

        observer.awaitTerminalEvent()
        observer.assertNoErrors()
        observer.assertValueCount(1)

        assertEquals(
            Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true),
            observer.values()[0]
        )
    }

    @Test
    fun execute_repositoryError_errorEventTriggered() {
        setupVehicleRepositoryWithFailedResponse()

        val observer: TestObserver<Vehicle> = TestObserver.create()
        getVehicleDetail.execute().subscribe(observer)

        observer.assertError(VehicleRepositoryException::class.java)
    }

    private fun setupVehicleRepositoryWithSuccessResponse() {
        whenever(vehicleRepository.get(eq(107623))).thenReturn(
            Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true)
        )
    }

    private fun setupVehicleRepositoryWithFailedResponse() {
        whenever(vehicleRepository.get(any())).thenThrow(VehicleRepositoryException())
    }

}