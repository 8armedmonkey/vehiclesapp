package nl.parkmobile.assignment.vehiclesapp.vehicles.domain

import org.junit.Assert.assertEquals
import org.junit.Test


class TestVehicleType {

    @Test
    fun from_validValue_vehicleTypeReturned() {
        assertEquals(VehicleType.CAR, VehicleType.from("Car"))
        assertEquals(VehicleType.TRUCK, VehicleType.from("Truck"))
        assertEquals(VehicleType.RV, VehicleType.from("RV"))
    }

    @Test(expected = IllegalArgumentException::class)
    fun from_invalidValue_exceptionThrown() {
        VehicleType.from("Foo")
    }

}