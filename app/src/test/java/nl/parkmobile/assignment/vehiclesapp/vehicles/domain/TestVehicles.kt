package nl.parkmobile.assignment.vehiclesapp.vehicles.domain

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test


class TestVehicles {

    private lateinit var vehicles: Vehicles

    @Before
    fun setUp() {
        vehicles = Vehicles(
            Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true),
            Vehicle(107523, "PARK02", "ES", "White", VehicleType.RV, false),
            Vehicle(107613, "PARK03", "NL", "Red", VehicleType.CAR, false)
        )
    }

    @Test
    fun iterator_validIteratorReturned() {
        vehicles.iterator().asSequence().forEachIndexed { index, vehicle ->

            when (index) {
                0 -> assertEquals(107623, vehicle.id)
                1 -> assertEquals(107523, vehicle.id)
                2 -> assertEquals(107613, vehicle.id)
            }
        }
    }

    @Test
    fun get_vehicleExists_vehicleReturned() {
        val vehicle = vehicles.get(107623)
        assertEquals(107623, vehicle?.id)
    }

    @Test
    fun get_vehicleNotExists_nullReturned() {
        assertNull(vehicles.get(0))
    }

}