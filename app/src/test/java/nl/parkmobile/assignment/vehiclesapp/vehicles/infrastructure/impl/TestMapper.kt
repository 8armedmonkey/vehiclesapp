package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl

import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles
import org.junit.Assert.assertEquals
import org.junit.Test


class TestMapper {

    @Test
    fun map_dtoVehicles_domainVehiclesReturned() {
        val expected = Vehicles(
            Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true),
            Vehicle(107523, "PARK02", "ES", "White", VehicleType.RV, false),
            Vehicle(107613, "PARK03", "NL", "Red", VehicleType.CAR, false),
            Vehicle(117623, "PARK04", "NL", "Black", VehicleType.CAR, false),
            Vehicle(107627, "PARK05", "NL", "Blue", VehicleType.TRUCK, false)
        )

        val actual = Mapper.map(
            DTO.Vehicles(arrayOf(
                DTO.Vehicle(107623, "PARK01", "GB", "Blue", "Truck", true),
                DTO.Vehicle(107523, "PARK02", "ES", "White", "RV", false),
                DTO.Vehicle(107613, "PARK03", "NL", "Red", "Car", false),
                DTO.Vehicle(117623, "PARK04", "NL", "Black", "Car", false),
                DTO.Vehicle(107627, "PARK05", "NL", "Blue", "Truck", false)
            ))
        )

        actual.forEach { vehicle ->
            assertEquals(expected.get(vehicle.id), vehicle)
        }
    }

    @Test
    fun map_dtoVehicle_domainVehicleReturned() {
        val expected = Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true)
        val actual = Mapper.map(DTO.Vehicle(107623, "PARK01", "GB", "Blue", "Truck", true))

        assertEquals(expected, actual)
    }

}