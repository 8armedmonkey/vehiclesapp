package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Response


class TestVehicleRepository {

    companion object {

        const val CACHE_MAX_AGE_MILLIS: Long = 1000

    }

    private lateinit var vehicleWebService: VehicleWebService
    private lateinit var vehicleRepository: VehicleRepository

    @Before
    fun setUp() {
        vehicleWebService = mock()
        vehicleRepository = VehicleRepositoryImpl(
            CACHE_MAX_AGE_MILLIS,
            vehicleWebService
        )
    }

    @Test
    fun cacheNotExpired_cachedDataReturned() {
        setupVehicleWebServiceWithSuccessResponse()
        vehicleRepository.getAll()
        vehicleRepository.getAll()

        verify(vehicleWebService, times(1)).getAll()
    }

    @Test
    fun cacheExpired_liveDataReturned() {
        setupVehicleWebServiceWithSuccessResponse()
        vehicleRepository.getAll()

        Thread.sleep(CACHE_MAX_AGE_MILLIS + 500)
        vehicleRepository.getAll()

        verify(vehicleWebService, times(2)).getAll()
    }

    @Test
    fun get_noCachedData_getAllPerformedFirst() {
        setupVehicleWebServiceWithSuccessResponse()

        val expected = Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true)
        val actual = vehicleRepository.get(107623)

        verify(vehicleWebService).getAll()
        assertEquals(expected, actual)
    }

    @Test
    fun get_cacheExpired_getAllPerformedFirst() {
        setupVehicleWebServiceWithSuccessResponse()
        vehicleRepository.getAll()

        Thread.sleep(CACHE_MAX_AGE_MILLIS + 500)
        vehicleRepository.get(107623)

        verify(vehicleWebService, times(2)).getAll()
    }

    @Test
    fun getAll___cachedDataAvailable_webServiceError___cachedDataReturned() {
        setupVehicleWebServiceWithFailedResponse()
        setupVehicleRepositoryWithExpiredCachedData()

        assertNotNull(vehicleRepository.getAll())
    }

    @Test
    fun get___cachedDataAvailable_webServiceError___cachedDataReturned() {
        setupVehicleWebServiceWithFailedResponse()
        setupVehicleRepositoryWithExpiredCachedData()

        assertNotNull(vehicleRepository.get(107623))
    }

    @Test(expected = VehicleRepositoryException::class)
    fun getAll___noCachedData_webServiceError___throwVehicleRepositoryException() {
        setupVehicleWebServiceWithFailedResponse()
        vehicleRepository.getAll()
    }

    @Test(expected = VehicleRepositoryException::class)
    fun get___noCachedData_webServiceError___throwVehicleRepositoryException() {
        setupVehicleWebServiceWithFailedResponse()
        vehicleRepository.get(107623)
    }

    private fun setupVehicleWebServiceWithSuccessResponse() {
        val call: Call<DTO.Vehicles> = mock()
        val response: Response<DTO.Vehicles> = Response.success(
            DTO.Vehicles(arrayOf(
                DTO.Vehicle(107623, "PARK01", "GB", "Blue", "Truck", true),
                DTO.Vehicle(107523, "PARK02", "ES", "White", "RV", false)
            ))
        )

        whenever(call.execute()).thenReturn(response)
        whenever(vehicleWebService.getAll()).thenReturn(call)
    }

    private fun setupVehicleWebServiceWithFailedResponse() {
        whenever(vehicleWebService.getAll()).thenThrow(Exception())
    }

    private fun setupVehicleRepositoryWithExpiredCachedData() {
        val vehiclesRepositoryImpl = vehicleRepository as VehicleRepositoryImpl

        vehiclesRepositoryImpl.cachedVehicles = Vehicles(
            Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true)
        )

        vehiclesRepositoryImpl.lastUpdatedMillis = 0
    }

}