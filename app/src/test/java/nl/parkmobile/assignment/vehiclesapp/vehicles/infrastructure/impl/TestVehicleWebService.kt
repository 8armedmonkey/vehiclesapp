package nl.parkmobile.assignment.vehiclesapp.vehicles.infrastructure.impl

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class TestVehicleWebService {

    private lateinit var vehicleWebService: VehicleWebService

    @Before
    fun setUp() {
        vehicleWebService = VehicleWebServiceFactory.create()
    }

    @Test
    fun getAll_validResponseIsReturned() {
        val expected = DTO.Vehicles(arrayOf(
            DTO.Vehicle(107623, "PARK01", "GB", "Blue", "Truck", true),
            DTO.Vehicle(107523, "PARK02", "ES", "White", "RV", false),
            DTO.Vehicle(107613, "PARK03", "NL", "Red", "Car", false),
            DTO.Vehicle(117623, "PARK04", "NL", "Black", "Car", false),
            DTO.Vehicle(107627, "PARK05", "NL", "Blue", "Truck", false)
        ))

        assertEquals(expected, vehicleWebService.getAll().execute().body())
    }

}