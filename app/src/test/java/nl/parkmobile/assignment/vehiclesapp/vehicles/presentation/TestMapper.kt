package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import nl.parkmobile.assignment.vehiclesapp.R
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles
import org.junit.Assert.assertEquals
import org.junit.Test


class TestMapper {

    @Test
    fun map_domainVehicles_presentationVehiclesReturned() {
        val expected = VehiclesPresentation(
            VehiclePresentation(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true, R.drawable.ic_truck),
            VehiclePresentation(107523, "PARK02", "ES", "White", VehicleType.RV, false, R.drawable.ic_rv),
            VehiclePresentation(107613, "PARK03", "NL", "Red", VehicleType.CAR, false, R.drawable.ic_car)
        )

        val actual = Mapper.map(
            Vehicles(
                Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true),
                Vehicle(107523, "PARK02", "ES", "White", VehicleType.RV, false),
                Vehicle(107613, "PARK03", "NL", "Red", VehicleType.CAR, false)
            )
        )

        actual.forEachIndexed { index, vehiclePresentation ->
            assertEquals(expected.getAt(index), vehiclePresentation)
        }
    }

    @Test
    fun map_domainVehicle_presentationVehicleReturned() {
        val expected = VehiclePresentation(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true, R.drawable.ic_truck)
        val actual = Mapper.map(Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true))

        assertEquals(expected, actual)
    }

}