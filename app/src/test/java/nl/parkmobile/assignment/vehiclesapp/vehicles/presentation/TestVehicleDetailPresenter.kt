package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx.SchedulingTransformer
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx.SchedulingTransformerTestImpl
import nl.parkmobile.assignment.vehiclesapp.vehicles.appservice.VehiclesAppService
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepositoryException
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType
import org.junit.Before
import org.junit.Test


class TestVehicleDetailPresenter {

    private lateinit var vehiclesAppService: VehiclesAppService
    private lateinit var schedulingTransformer: SchedulingTransformer
    private lateinit var presenter: VehicleDetailPresenter
    private lateinit var view: VehicleDetailView

    @Before
    fun setUp() {
        vehiclesAppService = mock()
        view = mock()
        schedulingTransformer = SchedulingTransformerTestImpl()

        presenter = VehicleDetailPresenter(view, 107623)
        presenter.vehiclesAppService = vehiclesAppService
        presenter.schedulingTransformer = schedulingTransformer
    }

    @Test
    fun start_getVehicleDetail_viewShowLoading() {
        setupGetVehicleDetailSuccess()
        presenter.start()

        verify(vehiclesAppService).get(eq(107623))
        verify(view).showLoading()
    }

    @Test
    fun start_getVehicleDetailSuccess_viewShowVehicleDetail() {
        setupGetVehicleDetailSuccess()
        presenter.start()

        verify(view).showVehicleDetail(any())
    }

    @Test
    fun start_getVehicleDetailFailed_viewShowError() {
        setupGetVehicleDetailFailed()
        presenter.start()

        verify(view).showError(any())
    }

    @Test
    fun onNetworkActive_getVehicleDetail() {
        setupGetVehicleDetailSuccess()
        presenter.onNetworkActive()

        verify(vehiclesAppService).get(eq(107623))
    }

    private fun setupGetVehicleDetailSuccess() {
        whenever(vehiclesAppService.get(any())).thenReturn(
            Observable.just(Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true))
        )
    }

    private fun setupGetVehicleDetailFailed() {
        whenever(vehiclesAppService.get(any())).thenReturn(
            Observable.error(VehicleRepositoryException())
        )
    }

}