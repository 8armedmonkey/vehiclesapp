package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class TestVehiclesPresentation {

    private lateinit var vehiclesPresentation: VehiclesPresentation

    @Before
    fun setUp() {
        vehiclesPresentation = VehiclesPresentation(
            VehiclePresentation(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true, 0),
            VehiclePresentation(107523, "PARK02", "ES", "White", VehicleType.RV, false, 0),
            VehiclePresentation(107613, "PARK03", "NL", "Red", VehicleType.CAR, false, 0)
        )
    }

    @Test
    fun getAt_index_itemAtIndexReturned() {
        assertEquals(
            VehiclePresentation(107523, "PARK02", "ES", "White", VehicleType.RV, false, 0),
            vehiclesPresentation.getAt(1)
        )
    }

    @Test
    fun size_collectionSizeReturned() {
        assertEquals(3, vehiclesPresentation.size())
    }

}