package nl.parkmobile.assignment.vehiclesapp.vehicles.presentation

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx.SchedulingTransformer
import nl.parkmobile.assignment.vehiclesapp.common.infrastructure.rx.SchedulingTransformerTestImpl
import nl.parkmobile.assignment.vehiclesapp.vehicles.appservice.VehiclesAppService
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicle
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleRepositoryException
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.VehicleType
import nl.parkmobile.assignment.vehiclesapp.vehicles.domain.Vehicles
import org.junit.Before
import org.junit.Test


class TestVehiclesPresenter {

    private lateinit var vehiclesAppService: VehiclesAppService
    private lateinit var schedulingTransformer: SchedulingTransformer
    private lateinit var presenter: VehiclesPresenter
    private lateinit var view: VehiclesView
    private lateinit var navigator: VehiclesNavigator

    @Before
    fun setUp() {
        vehiclesAppService = mock()
        view = mock()
        navigator = mock()
        schedulingTransformer = SchedulingTransformerTestImpl()

        presenter = VehiclesPresenter(view, navigator)
        presenter.vehiclesAppService = vehiclesAppService
        presenter.schedulingTransformer = schedulingTransformer
    }

    @Test
    fun start_getAllVehicles_viewShowLoading() {
        setupGetAllVehiclesSuccess()
        presenter.start()

        verify(vehiclesAppService).getAll()
        verify(view).showLoading()
    }

    @Test
    fun start_getAllVehiclesSuccess_viewShowAllVehicles() {
        setupGetAllVehiclesSuccess()
        presenter.start()

        verify(view).showVehicles(any())
    }

    @Test
    fun start_getAllVehiclesFailed_viewShowError() {
        setupGetAllVehiclesFailed()
        presenter.start()

        verify(view).showError(any())
    }

    @Test
    fun onVehicleSelected_vehicleId_navigatorGoToVehicleDetail() {
        setupGetAllVehiclesSuccess()
        presenter.onVehicleSelected(107623)

        verify(navigator).goToVehicleDetail(eq(107623))
    }

    @Test
    fun onNetworkActive_getAllVehicles() {
        setupGetAllVehiclesSuccess()
        presenter.onNetworkActive()

        verify(vehiclesAppService).getAll()
    }

    private fun setupGetAllVehiclesSuccess() {
        whenever(vehiclesAppService.getAll()).thenReturn(
            Observable.just(Vehicles(
                Vehicle(107623, "PARK01", "GB", "Blue", VehicleType.TRUCK, true),
                Vehicle(107523, "PARK02", "ES", "White", VehicleType.RV, false),
                Vehicle(107613, "PARK03", "NL", "Red", VehicleType.CAR, false)
            ))
        )
    }

    private fun setupGetAllVehiclesFailed() {
        whenever(vehiclesAppService.getAll()).thenReturn(
            Observable.error(VehicleRepositoryException())
        )
    }

}